import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx'
import { ActionSheetController, ToastController, Platform } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

//servicio para guardar en el ionic storage
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.scss'],
})
export class NoticiaComponent implements OnInit {

  @Input() noticia: Article;
  @Input() indice: number;
  constructor(private inAppBrowser: InAppBrowser,
    private actionShhetCtlr: ActionSheetController,
    private socialSharing: SocialSharing,
    private dataLocalService: DataLocalService,
    private toastController: ToastController,
    private platform: Platform) { }

  ngOnInit() { }

  navigatePage() {
    console.log(this.noticia.url);
    const browser = this.inAppBrowser.create(this.noticia.url, '_system');
  }

  async menuNoticia() {

    var buttonFav: any;
    if (this.dataLocalService.isFavorito == false) {
      buttonFav = {
        text: 'Favorito',
        icon: 'heart',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Favorito clickado');
          this.dataLocalService.guardarFavorito(this.noticia);
          this.mostrarToast('Guardado en favoritos');
        },
      }
    } else {
      buttonFav = {
        text: 'Borrar favorito',
        icon: 'trash',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Borrar favorito clickado');
          this.dataLocalService.borrarFavorito(this.noticia);
          this.mostrarToast('Borrado de Favoritos');
        },
      }
    }



    const actionSheet = await this.actionShhetCtlr.create({
      buttons: [
        {
          text: 'Compartir',
          icon: 'share',
          cssClass: 'action-dark',
          handler: () => {
            console.log('Compartir clickado');
            this.compartirNoticia();
          }
        },
        buttonFav,
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          cssClass: 'action-dark',
          handler: () => {
            console.log('Cancelar clickado');
          }
        }]
    });
    await actionSheet.present();

  }

  async mostrarToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color: "primary"
    });
    toast.present();
  }


  compartirNoticia() {
    if (this.platform.is('cordova')) {
      this.socialSharing.share(this.noticia.title, this.noticia.source.name, null, this.noticia.url);
    } else {
      if (navigator['share']) {
        navigator['share']({
          title: this.noticia.title,
          text: this.noticia.description,
          url: this.noticia.url,
        })
          .then(() => console.log('Successful share'))
          .catch((error) => console.log('Error sharing', error));
      } else {
        console.log('No se pudo compartir porque no se soporta');
      }

    }
  }
}
