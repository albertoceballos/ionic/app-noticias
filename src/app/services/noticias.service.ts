import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

import {RespuestaNews} from '../interfaces/interfaces';

import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})


export class NoticiasService {

  public apiUrl='https://newsapi.org/v2';
  public apiKey=environment.apiKey;
  public page:number;

  constructor(private _httpClient:HttpClient) {
    this.page=0;
   }

  getNews(){
    this.page++;
    return this._httpClient.get<RespuestaNews>(this.apiUrl+'/top-headlines?country=us&page='+this.page+'&apiKey='+this.apiKey);

  }

  getNewsByCategory(categoria:string,page:number){
    return this._httpClient.get<RespuestaNews>(this.apiUrl+'/top-headlines?country=us&page='+page+'&category='+categoria+'&apiKey='+this.apiKey);
  }
}
