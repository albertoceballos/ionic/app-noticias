import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';

import { Article } from '../interfaces/interfaces'

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  public noticias: Array<Article>;
  public isFavorito:boolean;
  constructor(private storage: Storage) {
    this.noticias = [];
    this.cargarFavoritos();
    this.isFavorito=false;
  }

  guardarFavorito(noticia:Article) {

    if(this.noticias!=null){
      var existe = this.noticias.find(not => not.title == noticia.title);
    }else{
      this.noticias=[];
    }
  
    if (!existe) {
      //guaradar noticia al principio del array
      this.noticias.unshift(noticia);
      //guradar noticia en el Storage
      this.storage.set('favoritos', this.noticias);
    }

  }

  cargarFavoritos(){
    this.storage.get('favoritos').then((favoritos) => {
      console.log('Noticias', favoritos);
      this.noticias=favoritos;
    });
  }

  borrarFavorito(noticia:Article){
    this.noticias=this.noticias.filter(noti=> noti.title!==noticia.title);
    this.storage.set('favoritos', this.noticias);
  }

  getIsfavorito(){
    return this.isFavorito;
  }
}
