import { Component } from '@angular/core';

import {DataLocalService} from '../../services/data-local.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public dataLocalService:DataLocalService) {}

  onClick(){
    this.dataLocalService.isFavorito=false;
    console.log(this.dataLocalService.getIsfavorito());
  }
  
  onClickFavs(){
    this.dataLocalService.isFavorito=true;
    console.log(this.dataLocalService.getIsfavorito());
  }

}
