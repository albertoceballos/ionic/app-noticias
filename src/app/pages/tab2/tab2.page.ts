import { Component, ViewChild, OnInit} from '@angular/core';
import { IonSegment, IonInfiniteScroll } from '@ionic/angular';

import {Article} from '../../interfaces/interfaces';
//servicio
import {NoticiasService} from '../../services/noticias.service';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  @ViewChild(IonSegment) ionSegment:IonSegment;
  @ViewChild(IonInfiniteScroll) infiniteScroll:IonInfiniteScroll;
  public categories:Array<string>;
  public noticias:Array<Article>;
  public category:string;
  public categoriesPage:Array<any>=[];
  public page:number;
  constructor(private _noticiasService:NoticiasService) {
    this.noticias=[];
    this.categories=['general','business','entertainment','health','science','sports','technology'];
    this.page=1;
    /*this.categories.forEach(cat => {
      this.categoriesPage.push({cat:cat,page:0});
    });
    console.log(this.categoriesPage);*/
  }

  ngOnInit(){
    this.ionSegment.value=this.categories[1];
    this.category=this.categories[1];
    this.getCategory(this.category,this.page);
  }

  clickCategory(evento){
    this.category=evento.detail.value;
    this.infiniteScroll.disabled=false;
    this.noticias=[];
    this.page=1;
    this.getCategory(this.category,this.page);
   
  }

  getCategory(cat,page){
    this._noticiasService.getNewsByCategory(cat,page).subscribe(
      response=>{
        this.noticias.push(...response.articles);
        if(response.articles.length<20){
          this.infiniteScroll.disabled=true;
        }
      },
      error=>{
        console.log(error);
      }
    );
  }

  loadData(evento){
    this.page++;
    setTimeout(()=>{
      this.getCategory(this.category,this.page);
      evento.target.complete();
    },1500);
  }

}
