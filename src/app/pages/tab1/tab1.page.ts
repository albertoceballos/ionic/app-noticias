import { Component, OnInit, ViewChild } from '@angular/core';

import { NoticiasService } from '../../services/noticias.service';

//interface
import { Article } from '../../interfaces/interfaces';
import { IonInfiniteScroll,NavController } from '@ionic/angular';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  public data: Array<any>;
  public noticias: Array<Article>;

  @ViewChild(IonInfiniteScroll) _InfiniteScroll: IonInfiniteScroll;
  constructor(private _noticiasService: NoticiasService) {
    this.noticias = [];
  }


  ngOnInit() {
    this.getNews();
  }

  getNews(evento?) {
    this._noticiasService.getNews().subscribe(
      response => {

        if(response.articles.length==0){
          evento.target.disabled=true;
          evento.target.complete();
          return;
        }
        this.noticias.push(...response.articles);
        if(evento){
          evento.target.complete();
        }
      },
      error => {
        console.log(error);
      }
    );
  }


  loadData(evento){
    console.log(evento);
    setTimeout(()=>{
      this.getNews(evento);
      evento.target.complete();
    },1000);
    
  }


}
